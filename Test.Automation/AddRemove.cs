﻿using System;
using System.Linq;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using Test.Automation.Pages;

namespace Test.Automation
{
    [TestClass]
    public class AddRemove
    {
        private RemoteWebDriver webDriver;

        private static readonly string XPATH_EDICION = "/html/body/div[5]/div[1]/div[3]/div[1]/ul/li[2]/a";

        [TestInitialize]
        public void IniciarDriver()
        {
            this.webDriver = new ChromeDriver("Drivers/");
        }

        [TestCleanup]
        public void EliminarElProcesoDelDriver()
        {
            this.webDriver.Close();
        }


        [TestMethod]
        public void Add10Elements()
        {
            var addRemoveElementsPage = new AddRemoveElements();
            this.webDriver.Navigate().GoToUrl("https://the-internet.herokuapp.com/add_remove_elements/");
            PageFactory.InitElements(this.webDriver, addRemoveElementsPage);
            Enumerable.Range(1, 20).ToList().ForEach(number =>
            {
                addRemoveElementsPage.AddElementButton.Click();
                Thread.Sleep(500);
            });
            var newButtons = addRemoveElementsPage.NewButtons;
            Assert.AreEqual(newButtons.Count, 20);
            newButtons.ToList().ForEach(button =>
            {
                button.Click();
                Thread.Sleep(500);
            });
        }
    }
}
